/*
 * AVL tree
 */
package tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import tax.TaxPayer;

/*
 *  author Nguyen Minh Quang HE130059
 *         Nguyen Quang Huy HE130069
 *         Pham Quang Huy HE130022
 */
public class AVLTree {
    Node root;
   
    public AVLTree() {
        root = null;
    }
    
    public boolean isEmpty() {
        return root == null;
    }
    
    public void clear() {
        root = null;
    }
    
    public void insert(TaxPayer x) {
        root = insert(root,x);
    }
    
    private Node insert(Node p, TaxPayer x) {
        if(p == null) p = new Node(x);
        //insert to left child
        else if(x.getCode().compareToIgnoreCase(p.info.getCode())<0) {
            p.left = insert(p.left,x);
            //left unbalance
            if(height(p.left)-height(p.right)==2) {
                Node t = p.left;
                //left left
                if(height(t.left)-height(t.right)>=0)
                    p = rightRotate(p);
                //left right
                else p = leftThenRightDoubleRotate(p);
            }
        //insert to right child
        }else if(x.getCode().compareToIgnoreCase(p.info.getCode())>0) {
            p.right = insert(p.right, x);
            //right unbalance
            if(height(p.left)-height(p.right)==-2) {
                Node t = p.right;
                //right right
                if(height(t.left)-height(t.right)<=0)
                    p = leftRotate(p);
                //right left
                else p = rightThenLeftDoubleRotate(p);
            }
        }
        return p;
    }
    
    private Node rightRotate(Node p) {
        Node t = p.left;
        p.left = t.right;
        t.right = p;
        return t;
    }
    
    private Node leftRotate(Node p) {
        Node t = p.right;
        p.right = t.left;
        t.left = p;
        return t;
    }
    
    private Node leftThenRightDoubleRotate(Node p) {
        p.left = leftRotate(p.left);
        return rightRotate(p);
    }
    
    private Node rightThenLeftDoubleRotate(Node p) {
        p.right = rightRotate(p.right);
        return leftRotate(p);
    }
    
    public int count() {
        return countNode(root);
    }
    
    private int countNode(Node p) {
        //if reach the leaves
        if(p == null) return 0;
        //not reach, recursive count
        else {
            int count = 1;
            count += countNode(p.left);
            count += countNode(p.right);
            return count;
        }
    }
    
    public Node search(String x) {
        return search(root,x);
    }
    
    private Node search(Node p, String x) {
        String currentCode = (p!=null)?p.info.getCode():null;
        //if found or reach leaves and not found
        if(x.equalsIgnoreCase(currentCode) || p == null) return p;
        //recursive search if have not found
        else if (x.compareToIgnoreCase(currentCode)<0) return search(p.left,x);
        else return search(p.right,x);
    }
    
    public ArrayList<TaxPayer> preOrder() {
        return preOrder(root);
    }
    
    private ArrayList<TaxPayer> preOrder(Node p) {
        ArrayList<TaxPayer> list = new ArrayList<>();
        //add preorder to a list
        if(p!= null) {
            //add taxpayer of current node visited
            list.add(p.info);
            //add all taxpayer of left tree to list
            list.addAll(preOrder(p.left));
            //add all taxpayer of right tree to list
            list.addAll(preOrder(p.right));
            return list;
        }else return list;
    }
    
    public ArrayList<TaxPayer> inOrder() {
        return inOrder(root);
    }
    
    private ArrayList<TaxPayer> inOrder(Node p) {
        ArrayList<TaxPayer> list = new ArrayList<>();
        //add inorder to a list
        if(p!= null) {
            //add all taxpayer of left tree to list
            list.addAll(inOrder(p.left));
            //add taxpayer of current node visited
            list.add(p.info);
            //add all taxpayer of right tree to list
            list.addAll(inOrder(p.right));
            return list;
        }else return list;
    }
    
    public ArrayList<TaxPayer> breadth() {
        //queue contain node
        Queue<Node> q = new LinkedList<>();
        //list contain tax payer
        ArrayList<TaxPayer> list = new ArrayList<>();
        q.add(root);
        //breadth-first traverse
        while(!q.isEmpty()) {
            Node temp = q.poll();
            list.add(temp.info);
            if(temp.left!=null) q.add(temp.left);
            if(temp.right!=null) q.add(temp.right);
        }
        return list;
    }
    
    private int height(Node p) {
        //reach leaves
        if(p==null) return 0;
        else {
            //height of left tree
            int lheight = height(p.left);
            //height of right tree
            int rheight = height(p.right);
            //return height of max(lheight,rheight)+1
            if(lheight>rheight) return (lheight+1);
            else return (rheight+1);
        }
    }
    
    private Node findMin(Node p) {
        if(p == null)
            return p;
        //find the left most node => min node
        while(p.left != null)
            p = p.left;
        return p;
    }
    
    public void remove(TaxPayer t) {
        root = remove(t, root);
    }
    
    private Node remove(TaxPayer x, Node p) {
        //Item not found; do nothing
        if(p == null)
            return p;   
        int compareResult = x.getCode().compareToIgnoreCase(p.info.getCode());
        if(compareResult < 0){
            p.left = remove(x, p.left);
            //right unbalance
            if(height(p.left)-height(p.right)==-2) {
                Node t = p.right;
                //right right
                if(height(t.left)-height(t.right)<=0)
                    p = leftRotate(p);
                //right left
                else p = rightThenLeftDoubleRotate(p);
            }
        }else if(compareResult > 0) {
            p.right = remove(x, p.right);
            //left unbalance
            if(height(p.left)-height(p.right)==2) {
                Node t = p.left;
                //left left
                if(height(t.left)-height(t.right)>=0)
                    p = rightRotate(p);
                //left right
                else p = leftThenRightDoubleRotate(p);
            }
        //two children
        }else if(p.left != null && p.right != null) {
            p.info = findMin(p.right).info;
            p.right = remove(p.info,p.right);
        //1 son or is leaf
        }else p = (p.left != null)?p.left:p.right;        
        return p;
    }
    
}
