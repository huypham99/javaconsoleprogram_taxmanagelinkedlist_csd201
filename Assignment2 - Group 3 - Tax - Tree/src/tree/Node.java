/*
 * Node of tree
 */
package tree;

import tax.TaxPayer;

/*
 * author Pham Quang Huy HE130022
 */
public class Node {
    TaxPayer info;
    Node left, right;
    
    public Node() {
        this.info = null;
        this.left = null;
        this.right = null;
    }

    public Node(TaxPayer info) {
        this.info = info;
        this.left = null;
        this.right = null;
    }
    
    public TaxPayer getInfo() {
        return this.info;
    }
}
