/*
 * tax payer
 */
package tax;

import java.text.DecimalFormat;

/*
 *
 * author Pham Quang Huy HE130022
 */
public class TaxPayer {
    private String code, name;
    private double income, deduct;

    public TaxPayer() {
    }

    public TaxPayer(String code, String name, double income, double deduct) {
        this.code = code;
        this.name = name;
        this.income = income;
        this.deduct = deduct;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getIncome() {
        return income;
    }

    public double getDeduct() {
        return deduct;
    }
    
    public double getTax() {
        double tax;
        double taxable = income - deduct;
        tax = taxable * (taxable <= 5000 ? 0.05 : (taxable <= 10000 ? 0.1 : 0.15));
        return tax;
    }

    @Override
    public String toString() {
        double tax = getTax();
        String infoLine="";
        infoLine = formatColumnGap(infoLine, code, 12);
        infoLine = formatColumnGap(infoLine, name, 17);
        infoLine = formatColumnGap(infoLine, format(income)+"", 15);
        infoLine = formatColumnGap(infoLine, format(deduct), 18);
        infoLine += format(tax);
        return infoLine;
    }
    
    private String format(double number) {
        String str;
        if(number-(int)number!=0)
            str = new DecimalFormat(".##").format(number);
        else
            str = (int)number + "";
        return str;
    }
    
    private String formatColumnGap(String infoLine, String content,int gap) {
        infoLine+=content;
        for (int i = 1; i <= gap-content.length(); i++) {
            infoLine+=" ";
        }
        return infoLine;
    }
    
    public void detail() {
        double tax = getTax();
        String taxStr = new DecimalFormat(".##").format(tax);
        System.out.println("-----------------Infomation------------------");
        System.out.println("Code: "+code);
        System.out.println("Name: "+name);
        System.out.println("Incode: "+income);
        System.out.println("Deduction: "+deduct);
        System.out.println("Tax: "+taxStr);
    }
}
