/*
 * tax payer tree extends AVL tree, contain functions which main calls
 */
package tax;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import tree.AVLTree;
import tree.Node;
import valid.ValidateData;

/*
 *  author Nguyen Minh Quang HE130059
 *         Hoang Duc Phong HE130088
 *         Nguyen Quang Huy HE130069
 *         Pham Quang Huy HE130022
 */
public class TaxPayerTree extends AVLTree{
    
    public void inorderToFile(String fname) {
        //if empty tree
        if(isEmpty()) {
            System.out.println("Tree is empty.");
            return;
        }
        ArrayList<TaxPayer> list = inOrder();
        try {
            File f = new File(fname);
            FileWriter fw = new FileWriter(f);
            PrintWriter pw = new PrintWriter(fw);
            //write all the list to file
            for (TaxPayer x: list) {
                pw.println(x.getCode() + "," + x.getName() + "," + x.getIncome() + "," +x.getDeduct());
            }
            pw.close();
            fw.close();
            System.out.println("Save successfully.");
        } catch (IOException e) {
            System.out.println("Save Error.");
        }
    }
    
    public void loadDataFromFile(String fname) {
        try {
            //if tree currently not empty, clear all
            if(!isEmpty()) {
                clear();
            }
            File f = new File(fname);
            FileReader fr = new FileReader(f);
            BufferedReader bf = new BufferedReader(fr);
            String details;
            //read each line of the file
            while ((details = bf.readLine()) != null) {
                //Split into String[]
                String[] info = details.split(",");
                String code = info[0];
                String name = info[1];
                double income = Double.parseDouble(info[2]);
                double deduct = Double.parseDouble(info[3]);
                TaxPayer t = new TaxPayer(code, name, income, deduct);
                insert(t);
            }
            System.out.println("Load successfully.");
        } catch (IOException | NumberFormatException e) {
            System.out.println("Load Error.");
        }
    }
    
    public void insertTaxPayer() {
        insert(createTaxPayer());
        System.out.println("Add successfully.");
    }
    
    public void inorder() {
        //if tree empty
        if(isEmpty()) {
            System.out.println("Tree is empty.");
            return;
        }
        display(inOrder());
    }
    
    public void preorder() {
        //if tree empty
        if(isEmpty()) {
            System.out.println("Tree is empty.");
            return;
        }
        display(preOrder());
    }
    
    public void breadthFirst() {
        //if tree empty
        if(isEmpty()) {
            System.out.println("Tree is empty.");
            return;
        }
        display(breadth());
    }
    
    public void searchByCode() {
        //if tree empty
        if(isEmpty()) {
            System.out.println("Tree is empty.");
            return;
        }
        String findCode;
        //valid search code
        do {
            System.out.print("Enter code to search: ");
            findCode = sc.nextLine();
        }while(!ValidateData.checkBlank(findCode));
        Node findNode = search(findCode);
        if(findNode==null) {
            System.out.println("This code is not exist in tree.");
        }else {
            findNode.getInfo().detail();
        }
    }
    
    public void deleteByCode() {
        //if tree empty
        if(isEmpty()) {
            System.out.println("Tree is empty.");
            return;
        }
        String deleteCode;
        //valid search code
        do {
            System.out.print("Enter code to delete: ");
            deleteCode = sc.nextLine();
        }while(!ValidateData.checkBlank(deleteCode));
        Node delNode = search(deleteCode);
        if(delNode==null) {
            System.out.println("This code is not exist in tree.");
        }else{
            remove(delNode.getInfo());
            System.out.println("Remove successfully.");
        }
    }
    
    public void countTaxPayer() {
        System.out.println("Current number of tax payer in tree: "+count());
    }
    
    public void display(ArrayList<TaxPayer> list) {
        //if list is currently empty
        if (isEmpty()) {
            System.out.println("Tree is empty!");
            return;
        }
        System.out.println("---------------------------------Tax Payers----"
                + "----------------------------");
        System.out.println("No.\tCode        Name             Income"
                + "         Deduction         Tax");
        for (int i = 0; i < list.size(); i++) {
            System.out.println((i+1) + "\t" + list.get(i).toString());
        }
    }
    
    private TaxPayer createTaxPayer() {
        String code, name, incomeStr, deduct;
        double income;
        //validate code
        do {
            System.out.print("Enter taxpayer's code: ");
            code = sc.nextLine();
        } while (!checkCode(code));
        //validate name
        do {
            System.out.print("Enter taxpayer's name: ");
            name = sc.nextLine();
        } while (!ValidateData.checkBlank(name));
        //validate incomeStr
        do {
            System.out.print("Enter income: ");
            incomeStr = sc.nextLine();
        } while (!ValidateData.checkPosDouble(incomeStr));
        income = Double.parseDouble(incomeStr);
        //validate deduct
        do {
            System.out.print("Enter deduction: ");
            deduct = sc.nextLine();
        } while (!checkDeduct(deduct, income));
        return new TaxPayer(code, name, income, Double.parseDouble(deduct));
    }

    private boolean checkCode(String code) {
        //not blank
        if (!ValidateData.checkBlank(code)) {
            return false;
        }
        //check existed
        if (search(code) != null) {
            System.out.println("This code has existed in list. Please enter again.");
            return false;
        }
        return true;
    }

    private boolean checkDeduct(String deductStr, double income) {
        //not blank
        if (!ValidateData.checkBlank(deductStr)) {
            return false;
        }
        //not double
        if (!ValidateData.isDouble(deductStr)) {
            System.out.println("This cannot contain non-digit character.");
            return false;
        }
        double deduct = Double.parseDouble(deductStr);
        //smaller than 0
        if (deduct < 0) {
            System.out.println("Deduction cannot be negative.");
            return false;
        }
        //greater than equal income
        if (deduct >= income) {
            System.out.println("Deduction must less than income.");
            return false;
        }
        return true;
    }

    static Scanner sc = new Scanner(System.in);
}
