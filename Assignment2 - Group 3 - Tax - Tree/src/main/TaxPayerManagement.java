/*
 * class contain main
 */
package main;

import java.util.Scanner;
import menu.Menu;
import tax.TaxPayerTree;
import valid.ValidateData;

/*
 * author Pham Quang Huy HE130022
 */
public class TaxPayerManagement {
        //scanner to get input of user
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        //list of taxpayer
        TaxPayerTree tree = new TaxPayerTree();
        //file name
        String fileName = "taxes.txt";
        Menu mainMenu = new Menu();
        setMenu(mainMenu);
        int choice;
        //repeat until user choose EXIT
        do {
            //display menu
            mainMenu.displayMenu();
            //get choice of user
            choice = mainMenu.getUserChoice();
            //execute choice of user
            switch(choice) {
                case 1:
                    tree.loadDataFromFile(fileName);
                    break;
                case 2:
                    tree.insertTaxPayer();
                    break;
                case 3:
                    tree.inorder();
                    break;
                case 4:
                    tree.preorder();
                    break;
                case 5:
                    tree.breadthFirst();
                    break;
                case 6:
                    tree.inorderToFile(fileName);
                    break;
                case 7:
                    tree.searchByCode();
                    break;
                case 8:
                    tree.deleteByCode();
                    break;
                case 9:
                    tree.countTaxPayer();
                    break;
                default:
                    System.out.println("Thanks for using!");
            }
            System.out.println("");
        }while(choice >= 1 && choice <= 9);
    }
    
    static void setMenu(Menu menu) {
        menu.setTitle("Tax Management");
        menu.addItem("Load data from file");
        menu.addItem("Input & insert data (insertion in AVL tree)");
        menu.addItem("In-order traverse");
        menu.addItem("Pre-order traverse");
        menu.addItem("Breadth-first traverse");
        menu.addItem("In-order traverse to file");
        menu.addItem("Search by code");
        menu.addItem("Delete by code (deletion in AVL tree)");
        menu.addItem("Count number of taxpayers");
    }
}
