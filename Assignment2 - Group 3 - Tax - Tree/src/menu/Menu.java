/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.util.ArrayList;
import java.util.Scanner;
import valid.ValidateData;

/**
 *
 * @author MSI
 */
public class Menu {
    ArrayList<String> list;
    String title;
    
    public Menu() {
        this.list = new ArrayList<>();
    }
    
    public Menu(String title) {
        this.list = new ArrayList<>();
        this.title = title;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public void addItem(String item) {
        list.add(item);
    }
    
    public int getNumberItems() {
        return list.size();
    }
    
    public void displayMenu() {
        System.out.println("======================== "+title.toUpperCase()+" ========================");
        int i = 1;
        for (String item: list) {
            System.out.println(i+". "+item);
            i++;
        }
        System.out.println("0. Exit");
        System.out.print("Your selection(0 -> "+(i-1)+"): ");
    }
    
    public int getUserChoice() {
        String choiceStr;
        //repeat if user enter invalid choice
        do {
            choiceStr = new Scanner(System.in).nextLine();
        }while(!ValidateData.checkOption(choiceStr,0,getNumberItems()));
        return Integer.parseInt(choiceStr);
    }
}