/*
 * validate input of user
 */
package valid;

/*
 *  author Pham Quang Huy HE130022
 */
public class ValidateData {
    static public boolean checkBlank(String string) {
        if(string.trim().isEmpty()) {
            System.out.println("This cannot blank!");
            return false;
        }
        return true;
    }
    
    static public boolean isInt(String string) {
        try {
            int temp = Integer.parseInt(string);
        }catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
    
    static public boolean isDouble(String string) {
        try {
            double temp = Double.parseDouble(string);
        }catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
    
    static public boolean checkOption(String choiceStr, int min, int max) {
        if(!checkBlank(choiceStr)) return false;
        //check exception if user enter non-integer
        if(!isInt(choiceStr)) {
            System.out.println("This cannot contain non-digit character");
            return false;
        }
        int option=Integer.parseInt(choiceStr);
        //if option is not in menu (out of range [min,max])
        if(option < min || option > max) {
            System.out.println("Option "+option+" not in menu");
            return false;
        }
        return true;
    }
    
    static public boolean checkPosInt(String posIntStr) {
        //check exception if user enter non-integer
        if(!isInt(posIntStr)) {
            System.out.println("This cannot contain non-digit character");
            return false;
        }
        int posInt=Integer.parseInt(posIntStr);
        if(posInt <= 0) {
            System.out.println("This must be greater than 0");
            return false;
        }
        return true;
    }
    
    static public boolean checkPosDouble(String posDouble) {
        //check exception if user enter non-double
        if(!isDouble(posDouble)) {
            System.out.println("This cannot contain non-digit character");
            return false;
        }
        if(Double.parseDouble(posDouble) <= 0) {
            System.out.println("This must be greater than 0");
            return false;
        }
        return true;
    }
}
