# Data Structure and Algorithm Assignment - Create Linked List and Tree Data Structure Console Program

These assignments contain 2 parts: **linked-list-structure** and **avl-tree-structure** works, using **Java** with **NetBeans IDE**

### Specification

* Download and open 2 **.html** files located at root for more details

### How to run

* Assignment 1 - Linked List : execute file ****~/Assignment1 - Group 3 - Tax - Linked List/dist/Program.jar****
* Assignment 2 - AVL Tree : execute file ****~/Assignment2 - Group 3 - Tax - Tree/dist/Program.jar****

